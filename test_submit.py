def create_submission(predictions, f='predictions.csv'):
    out = open(f, 'w')
    out.write("id,label\n")
    for i,p in enumerate(predictions):
        out.write("{},{}\n".format(i,p))

    out.close()
    return

import numpy as np

pred = np.array([0,1,2,3,4,5,6,7,8])
create_submission(pred)