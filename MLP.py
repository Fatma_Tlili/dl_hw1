from preprocess import *
from keras.callbacks import ModelCheckpoint
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
import numpy as np

import sys

os.environ['CUDA_VISIBLE_DEVICES'] = '0,1,2,3'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def simple_MLP(input_size, num_classes):
    model = Sequential()
    model.add(Dense(2048, input_shape=(input_size,), activation='relu'))
    
    model.add(Dense(1024, activation='relu'))
    model.add(BatchNormalization())
    
    model.add(Dense(1024, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    
    model.add(Dense(1024, activation='relu'))
    model.add(BatchNormalization())
    
    
    model.add(Dense(512, activation='linear'))
    model.add(LeakyReLU(alpha=.001))
    model.add(BatchNormalization())
    model.add(Dropout(0.1))
    
    model.add(Dense(num_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model


def create_submission(predictions, f='predictions.csv'):
    out = open(f, 'w')
    out.write("id,label\n")
    for i,p in enumerate(predictions):
        res = np.argmax(p)
        out.write("{},{}\n".format(i,res))

    out.close()
    return


def train(c, batch_size, weight=None):
    model = simple_MLP((2*c.k+1)*40, c.num_classes)
    model.summary()


    if weight != None:
        model.load_weights(weight)
    #    return model

    # generators
    train_gen = train_generator(c, batch_size)
    val_gen = val_generator(c, batch_size)


    filepath="models/weight{epoch:02d}-{val_acc:.2f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')



    # Fit the model
    model.fit_generator(train_gen, steps_per_epoch= (c.tr_len/batch_size), 
        epochs=20, validation_data=val_gen, validation_steps= (c.val_len/batch_size), callbacks=[checkpoint])

    return model




if __name__ == "__main__":
    source = "data/"
    k = 25
    batch_size = 512

    pre_processor = preprocess(source,k)

    if len(sys.argv) > 1:
        model_file = sys.argv[1]
        model = train(pre_processor, batch_size, weight=model_file)

    else:
        model = train(pre_processor, batch_size)


    # one batch for all the test set
    test_gen = test_generator(pre_processor, pre_processor.test_len)
    predictions = model.predict_generator(test_gen, steps=1)
    # print (predictions, batch_size)
    create_submission(predictions)




# nohup








