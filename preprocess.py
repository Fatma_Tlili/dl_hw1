from numpy.random import seed
seed(7)
from tensorflow import set_random_seed
set_random_seed(2)
# fix random seed for reproducibility

from keras.preprocessing.image import array_to_img, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
import sys
import os
import numpy as np

class WSJ():
  
    def __init__(self, source):
        self.source = source
        self.num_classes = None
        # tuple of data and labels
        self.train_set = None
        # tuple of data and labels
        self.dev_set = None
        # tuple of data and None
        self.test_set = None
        # context
        self.k = 0


        # matrix of all padded training data
        self.tr_data = None
        # vector of all training labels
        self.tr_labels = None
        # len of train data (no padding)
        self.tr_len = None
        # mapping between padded and non padded train data
        self.tr_mapping = None


        # matrix of all padded validation data
        self.val_data = None
        # vector of all validation labels
        self.val_labels = None
        # len of val data (no padding)
        self.val_len = None
        # mapping between padded and non padded val data
        self.val_mapping = None


        # matrix of all padded test data
        self.test_data = None
        # len of test data (no padding)
        self.test_len = None
        # mapping between padded and non padded test data
        self.test_mapping = None
  
    @property
    def dev(self):
        if self.dev_set is None:
            self.dev_set = load_raw(self.source, 'dev')
        return self.dev_set

    @property
    def train(self):
        if self.train_set is None:
            self.train_set = load_raw(self.source, 'train')
        return self.train_set
  
    @property
    def test(self):
        if self.test_set is None:
            self.test_set = (np.load(os.path.join(self.source, 'test.npy'), encoding='bytes'), None)
        return self.test_set

    

def load_raw(path, name):
    return (
        np.load(os.path.join(path, '{}.npy'.format(name)), encoding='bytes'), 
        np.load(os.path.join(path, '{}_labels.npy'.format(name)), encoding='bytes'))


def pad(set_t, k):
        padd_tr_set = [None for idx in range(len(set_t[0]))]
        # padding
        num_frames = 0
        for i, uttr in enumerate(set_t[0]):
            num_frames += len(uttr)
            padd_uttr = np.pad(uttr, ((k,k),(0,0)), 'mean')
            padd_tr_set[i] = padd_uttr
            if set_t[1] is not None:
                d = (set_t[1][i]).shape[0]
                set_t[1][i] = (set_t[1][i]).reshape(d,1)

        data = np.vstack(padd_tr_set)
        if set_t[1] is not None:
            labels = np.vstack(set_t[1])
        else: 
            labels = None

        return (data, labels, num_frames)



def map_padded_data(utterances, k):
    l = [0 for i in range(len(utterances))]
    total_len = 0
    for i, uttr in enumerate(utterances):
        temp = np.arange(len(uttr))
        temp = temp + ((2*i+1)*k + total_len)
        l[i] = temp
        total_len += len(uttr)
    return np.concatenate(l)



def extract_batch_data(batch_data_indices, data_set, k):
    B = [0 for i in range(len(batch_data_indices))]
    for i, n in enumerate(batch_data_indices):
        temp = data_set[n-k : n+k+1, :]
        # this should be (2k+1 X 40)
        temp = temp.reshape(1,(2*k+1)*40)
        # this should be (1 X (2k+1).40)
        B[i] = temp
    return np.concatenate(B)




def train_generator(c, batch_size):
    labels_indices = np.arange(c.tr_len)
    data_indices = c.tr_mapping
    np.random.shuffle(labels_indices)
    data_indices = data_indices[labels_indices]
    i = 0
    while True:
        s = i%c.tr_len
        e = (i+batch_size)%c.tr_len
        if s >= e:
            batch_label_indices = np.concatenate((labels_indices[s:c.tr_len], labels_indices[0:e]), axis = 0)
            batch_data_indices = np.concatenate((data_indices[s:c.tr_len], data_indices[0:e]), axis = 0)
        else :
            batch_label_indices = labels_indices[s:e]
            batch_data_indices = data_indices[s:e]

        D = extract_batch_data(batch_data_indices, c.tr_data, c.k)
        # print ("3|", D.shape)
        # this should be (batch_size X (2k+1).40)
        L = c.tr_labels[batch_label_indices]
        # print ("4|", L.shape)
        # this should be (batch_size X 1)        
        # one hot encoding:
        Temp = np.zeros((len(L), c.num_classes), dtype = 'int64')
        # Temp[np.arange(len(L)), L] = 1

        # DEBUG:
        for lidx in range(len(L)):
            Temp[lidx, L[lidx]] = 1


        L = Temp
        # print ("5|", L.shape)
        # this should be (batch_size X 138)
        yield (D, L)
        i += batch_size



def val_generator(c, batch_size):
    labels_indices = np.arange(c.val_len)
    data_indices = c.val_mapping
    np.random.shuffle(labels_indices)
    data_indices = data_indices[labels_indices]
    i = 0
    while True:
        s = i%c.val_len
        e = (i+batch_size)%c.val_len
        if s >= e:
            batch_label_indices = np.concatenate((labels_indices[s:c.val_len], labels_indices[0:e]), axis = 0)
            batch_data_indices = np.concatenate((data_indices[s:c.val_len], data_indices[0:e]), axis = 0)
        else :
            batch_label_indices = labels_indices[s:e]
            batch_data_indices = data_indices[s:e]

        D = extract_batch_data(batch_data_indices, c.val_data, c.k)
        # print ("3|", D.shape)
        # this should be (batch_size X (2k+1).40)
        L = c.val_labels[batch_label_indices]
        # print ("4|", L.shape)
        # this should be (batch_size X 1)
        Temp = np.zeros((len(L), c.num_classes), dtype = 'int64')
        # Temp[np.arange(len(L)), L] = 1

        # DEBUG:
        for lidx in range(len(L)):
            Temp[lidx, L[lidx]] = 1
        L = Temp
        # print ("5|", L.shape)
        # this should be (batch_size X 138)
        yield (D, L)
        i += batch_size



def test_generator(c, batch_size):
    data_indices = c.test_mapping
    # np.random.shuffle(data_indices)
    i = 0
    while True:
        s = i%c.test_len
        e = (i+batch_size)%c.test_len
        if s >= e:
            batch_data_indices = np.concatenate((data_indices[s:c.test_len], data_indices[0:e]), axis = 0)
        else :
            batch_data_indices = data_indices[s:e]

        D = extract_batch_data(batch_data_indices, c.test_data, c.k)
        # print ("3|", D.shape)
        # this should be (batch_size X (2k+1).40)
        yield D
        i += batch_size



def preprocess(source,k):
    pre_processor = WSJ(source)
    pre_processor.train
    pre_processor.dev
    pre_processor.test
    pre_processor.k = k


    # fill in the class with padded data
    if (pre_processor.tr_data is None) and (pre_processor.tr_labels is None):
        (pre_processor.tr_data, pre_processor.tr_labels, pre_processor.tr_len) =  pad(pre_processor.train_set, pre_processor.k)

    if (pre_processor.val_data is None) and (pre_processor.val_labels is None):
        (pre_processor.val_data, pre_processor.val_labels, pre_processor.val_len) =  pad(pre_processor.dev_set, pre_processor.k)

    if pre_processor.test_data is None:
        (pre_processor.test_data, _, pre_processor.test_len) =  pad(pre_processor.test_set, pre_processor.k)


    pre_processor.num_classes = np.amax(pre_processor.tr_labels) + 1

    # map indices
    pre_processor.tr_mapping = map_padded_data(pre_processor.train_set[0], pre_processor.k)
    pre_processor.val_mapping = map_padded_data(pre_processor.dev_set[0], pre_processor.k)
    pre_processor.test_mapping = map_padded_data(pre_processor.test_set[0], pre_processor.k)

    # get rid of the unprocessed data
    pre_processor.train_set = None
    pre_processor.dev_set = None
    pre_processor.test_set = None

    return pre_processor







